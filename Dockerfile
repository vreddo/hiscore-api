FROM ubuntu:18.04

ENV TZ=Australia/Brisbane
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y apache2

RUN apt-get install -y php libapache2-mod-php php-mysql

COPY dir.conf /etc/apache2/mods-enabled
COPY apache2.conf /etc/apache2

COPY .htaccess /var/www/html
COPY *.php /var/www/html/
RUN mkdir /var/www/html/v1
COPY v1/. /var/www/html/v1/.

RUN a2enmod rewrite

ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

