<?php

// Connect to database
include("../connection.php");
$db = new dbObj();
$connection =  $db->getConnstring();
 
$request_method=$_SERVER["REQUEST_METHOD"];

switch($request_method) {
    case 'GET':
        if(!empty($_GET["id"]))
        {
            $id=intval($_GET["id"]);
            get_minigame($id);
        }
        else
        {
            get_minigame(0);
        }
        break;
    case 'POST':
        add_minigame();
        break;
    default:
        // Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
    
function get_minigame($id=0) {
    global $connection;
    
    $query = "SELECT * FROM minigames";
    
    if($id > 0)
    {
        $query .= " WHERE id = " . $id . " LIMIT 1";
    }
    
    $response = array();
    $result = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_assoc($result))
    {
        $response[] = $row;
    }
    
    header('Content-Type: application/json');
    echo json_encode($response);
}

function add_minigame() {
    global $connection;

    $data = json_decode(file_get_contents('php://input'), true);

    if(!empty($data["name"])) {
        $query = "INSERT INTO minigames SET name='" . $data["name"] . "'";
            if(mysqli_query($connection, $query)) {
                $response = array('status' => 1, 'status_message' => 'Minigame added.');
            } else {
                $response = array('status' => 0, 'status_message' => 'Minigame addition failed.');
            }
    } else {
        $response = array('status' => 0, 'status_message' => 'Minigame data incomplete.');
    }

    header('Content-Type: application/json');
    echo json_encode($response);
}

?>
