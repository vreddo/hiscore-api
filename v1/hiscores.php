<?php

// Connect to database
include("../connection.php");
$db = new dbObj();
$connection =  $db->getConnstring();
 
$request_method=$_SERVER["REQUEST_METHOD"];

switch($request_method) {
    case 'GET':
        get_scores($_GET["id"], $_GET["mode"]);
        break;
    case 'POST':
        add_hiscore();
        break;
    default:
        // Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
    
function get_scores($id=0, $mode="") {
    global $connection;
    
    $query = "SELECT * FROM high_scores";
    
    if(!empty($mode) && (($mode == "topten") || ($mode == "all"))) {
        if($id > 0) {
            
            $query .= " WHERE gameid = ". $id . " ORDER BY score DESC";
            
            if($mode == "topten") {
                $query .= " LIMIT 10";
            }

            $response = array();
            $result = mysqli_query($connection, $query);
            
            while($row = mysqli_fetch_assoc($result)) {
                $response[] = $row;
            }           
        } else {
            $response = array('status' => 0, 'status_message' => 'Invalid minigame id.');   
        }
    } else {
        $response = array('status' => 0, 'status_message' => 'Invalid mode.');
    }

    header('Content-Type: application/json');
    echo json_encode($response);
}

function add_hiscore() {
    global $connection;

    $data = json_decode(file_get_contents('php://input'), true);

    if(
        !empty($data["gameid"]) && 
        !empty($data["userid"]) &&
        !empty($data["score"])
    ) {
            $query = "INSERT INTO high_scores SET ";
            $query .= "gameid = " . $data["gameid"] . ", ";
            $query .= "userid = " . $data["userid"] . ", ";
            $query .= "score = " . $data["score"];

            if(mysqli_query($connection, $query)) {
                $response = array('status' => 1, 'status_message' => 'Game score added.');
            } else {
                $response = array('status' => 0, 'status_message' => 'Game score addition failed.');
            }
    } else {
        $response = array('status' => 0, 'status_message' => 'Game score data incomplete.');
    }

    header('Content-Type: application/json');
    echo json_encode($response);
}

?>
